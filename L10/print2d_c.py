import numpy as np
import ctypes as ct # for calling C from Python
lib = ct.cdll.LoadLibrary("./print2d.so") # load C print2d function

# create 2d array
A = np.array([[ 1, 2, 3, 4 ], [ 5, 6, 7, 8 ], [ 9, 10, 11, 12 ] ],dtype='int32')
(rows,cols) = A.shape

# print 2d array using C function
A_cptr = A.ctypes.data_as(ct.POINTER(ct.c_int32))
lib.print2d(A_cptr,ct.c_int(rows),ct.c_int(cols))
