#include <stdio.h>

int main () {
    double next;
    double sum = 0.;
    int count = 0;
    while (scanf("%lf",&next) == 1) {
        sum += next;
        count += 1;
    }
    float average = sum/count;
    printf ("average = %.2f\n",average);
}
