#include <stdio.h>
#include <stdlib.h>

int main () {

    // allocate an array of 5 integers on the stack
    int a[5];

    // allocate an array of 5 integers on the heap
    // malloc stands for "memory allocation"
    // memory allocated using malloc is not initialized.
    int* b = (int*)malloc(5*sizeof(int));

    // allocate an array of 5 integers on the stack
    // and initialize the array to contain all 0s.
    int c[5] = { 0 };

    // allocate an array of 5 integers on the heap
    // and initialize the array to contain all 0s.
    // calloc stands for "clear allocate"
    // note that the interface to calloc differs slightly
    int* d = (int*)calloc(5,sizeof(int));

    // all of these arrays can be used in the same way
    a[2] = 10;
    b[2] = 2*a[2];
    c[2] += a[2]+b[2];
    d[2] += c[2]+a[2];
    printf ("%d %d %d %d\n",a[2],b[2],c[2],d[2]);

    // Memory for stack variables is automatically
    // freed when it is no longer needed.
    // Memory for heap variables is freed manually.
    // Do not continue to use memory after it has been freed!
    free(b);
    free(d);
}
