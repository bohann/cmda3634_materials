#include <stdio.h>
#include "pri_queue.h"

int main () {

    /* read the number of entries in the file */
    int num;
    if (scanf("%*c %d",&num) != 1) {
        printf ("error reading the number of entries!\n");
        return 1;
    }

    /* initialize the priority queue */
    pri_queue pq;
    pri_queue_init (&pq,num);

    /* read entries from stdin and add them to the priority queue */
    pri_queue_element next;
    for (int i=0;i<num;i++) {
        if (scanf("%lf",&(next.priority)) != 1) {
            printf ("error : file too small\n");
            return 1;
        }
        pri_queue_insert (&pq,next);
    }

    /* print the entries from largest to smallest */
    while (pq.cur_size > 0) {
        next = pri_queue_peek_top(&pq);
        printf ("%g\n",next.priority);
        pri_queue_delete_top(&pq);
    }

    /* free the priority queue */
    pri_queue_free (&pq);
}
