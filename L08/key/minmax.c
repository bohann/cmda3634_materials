#include <stdio.h>
#include <float.h>
#include <math.h>
#include "vec.h"

int main () {

    // read the number of points and the dimension of each point
    int num_points, dim;
    if (scanf("%*c %d %d",&num_points, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    double max_norm_sq = 0;
    double min_norm_sq = DBL_MAX;
    int closest, farthest;
    double next[dim];
    // Read vectors from stdin and keep track of closest/farthest indices
    for (int i=0;i<num_points;i++) {
        if (vec_read_stdin(next,dim) != dim) {
            printf ("error reading the next point from stdin\n");
            return 1;
        }
        double norm_sq = vec_norm_sq(next,dim);
        if (norm_sq > max_norm_sq) {
            max_norm_sq = norm_sq;
            farthest = i;
        }
        if (norm_sq < min_norm_sq) {
            min_norm_sq = norm_sq;
            closest = i;
        }
    }

    // print the results
    printf ("closest has length %.4f and index %d\n",sqrt(min_norm_sq),closest);
    printf ("farthest has length %.4f and index %d\n",sqrt(max_norm_sq),farthest);
}
