#include <stdio.h>

// v = 0
void vec_zero (double v[], int dim) {
    for (int i=0;i<dim;i++) {
        v[i] = 0;
    }
}

// read a vector from stdin
// returns the number of elements read in
int vec_read_stdin (double v[], int dim) {
    for (int i=0;i<dim;i++) {
        if (scanf("%lf",&(v[i])) != 1) { // could also use v+i
            return i;
        }
    }
    return dim;
}

// w = u + v
void vec_add (double u[], double v[], double w[], int dim) {
    for (int i=0;i<dim;i++) {
        w[i] = u[i] + v[i];
    }
}

// w = cv
void vec_scalar_mult (double v[], double c, double w[], int dim) {
    for (int i=0;i<dim;i++) {
        w[i] = c*v[i];
    }
}

// print a vector using the given format specifier
void vec_print (double v[], char* format, int dim) {
    for (int i=0;i<dim;i++) {
        printf (format,v[i]);
    }
    printf ("\n");
}

int main () {

    // read the number of points and the dimension of each point
    int num_points, dim;
    if (scanf("%*c %d %d",&num_points, &dim) != 2) {
        printf ("error reading the number of points and the dimension\n");
        return 1;
    }

    // The size of a C array can be specified using a variable.
    // Variable size arrays in C cannot be initialized with one line.
    // double sum[dim] = { 0 }; (this will not compile)
    double sum[dim];
    double average[dim];
    double next[dim];

    // We must initialize variable sized arrays using a loop.
    // To improve readability of the code we use a function.
    vec_zero(sum,dim);

    // Read vectors from stdin and accumulate the sum
    for (int i=0;i<num_points;i++) {
        if (vec_read_stdin(next,dim) != dim) {
            printf ("error reading the next point from stdin\n");
            return 1;
        }
        vec_add (sum,next,sum,dim);
    }

    // divide the sum by the number of points to get the average
    vec_scalar_mult(sum,1.0/num_points,average,dim);

    // print the results
    vec_print (average,"%.2f ",dim);
}
