#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char *argv[]) {

    // get N and num_threads from command line
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }
    long long N = atoll(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);

    // start the timer
    double start_time, end_time;
    start_time = omp_get_wtime();

    // calculate the sum
    long long sum = 0;
    for (long long i = 1; i <= N;i++) {
        sum += i;
    }

    // stop the timer
    end_time = omp_get_wtime();

    printf ("num_threads = %d, ",num_threads);
    printf ("elapsed time = %.6f\n",end_time-start_time);
    printf ("sum = %lld\n",sum);
    printf ("N*(N+1)/2 = %lld\n",(N/2)*(N+1));
}
