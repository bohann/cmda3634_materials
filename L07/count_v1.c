#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"n");
        return 1;
    }
    int n = atoi(argv[1]);
    int num_pairs = 0;
    for (int i=0;i<n;i++) {
        for (int j=0;j<n;j++) {
            if (i<j) {
                num_pairs += 1;
            }
        }
    }
    printf ("There are %d possible pairs of %d points.\n",num_pairs,n);
}
