#include <stdio.h>

typedef struct vec100k_s {
    double x[100000];
} vec100k;

vec100k vec100k_set (vec100k a, int i, double b) {
    a.x[i] = b;
    return a;
}

int main () {
    vec100k v;
    for (int i=0;i<100000;i++) {
        v = vec100k_set (v,i,i);
    }
    printf ("x[12345] = %.2f",v.x[12345]);
}
