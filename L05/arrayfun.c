#include <stdio.h>

// arrays in C are always passed by pointer
void fun1 (int* b) {
    b[0] = 3;
}

// the following alternate syntax is commonly used.
// both methods of passing an array are equivalent.
void fun2 (int c[]) {
    c[1] = 4;
}

int main () {
    int a[2] = { 1, 2 };
    printf ("a = [ %d %d ]\n\n",a[0],a[1]);

    fun1(a); // pass a pointer to the beginning of a
    printf ("a = [ %d %d ]\n\n",a[0],a[1]);

    fun2(a); // pass a pointer to the beginning of a
    printf ("a = [ %d %d ]\n\n",a[0],a[1]);
}
