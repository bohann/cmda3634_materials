#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {
    if (argc < 2) {
        printf ("command usage: %s %s\n",argv[0],"size");
        return 1;
    }
    int size = atoi(argv[1]);
    int A[size]; // starting in C99 array size can be a variable
    printf ("an int uses %ld bytes of storage\n",sizeof(int));
    for (int i=0;i<size;i++) {
        A[i] = i+1;
    }
    printf ("last element of A is %d\n",A[size-1]);
}
