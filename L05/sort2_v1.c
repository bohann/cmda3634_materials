#include <stdio.h>
#include <stdlib.h>

void swap (int a, int b) {
    int temp = a;
    a = b;
    b = temp;
}

int main (int argc, char* argv[]) {
    if (argc < 3) {
        printf ("command usage: %s %s %s\n",argv[0],"a","b");
        return 1;
    }
    int a = atoi(argv[1]);
    int b = atoi(argv[2]);
    if (b < a) {
        swap(a,b);
    }
    printf ("here are your two numbers sorted: %d %d\n",a,b);
}
