#include <stdio.h>
#include <stdlib.h> // for srandom and random functions
#include <time.h> // for time function

void random3 () {
    long long x = random();
    long long y = random();
    long long z = random();
    long long sum = x + y + z;
    printf ("here is the sum of three random integers : %lld\n\n",sum);
}

void fun_array () {
    int d[6]; // warning: arrays in C are not automatically initialized!
    printf ("d = [ ");
    for (int i=0;i<6;i++) {
        printf ("%d ",d[i]);
    }
    printf ("]\n\n");
}

int main () {
    srandom(time(NULL)); // seed random number generator
    fun_array();
    random3();
    fun_array();
}
